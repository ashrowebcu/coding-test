import math, sys

def circleArea(radius):
    return math.pi * radius ** 2
def sphereVolume(radius):
    return (4/3) * math.pi * radius ** 3

def convertFloat(input):
    try:
        return float(input)
    except ValueError:
        print('that\'s not a number!')
        

if __name__ == "__main__":
    print('Select a shape:')
    print('  1. Circle')
    print('  2. Sphere')
    input1 = input('Selection: ')
    if input1 == '1':
        print('Type in a radius:')
        input2 = input('Radius: ')
        converted = convertFloat(input2)
        if converted is None:
            sys.exit(0)
        print(circleArea(converted))
    elif input1 == '2':
        print('Type in a radius:')
        input2 = input('Radius: ')
        converted = convertFloat(input2)
        if converted is None:
            sys.exit(0)
        print(sphereVolume(converted))
    else:
        print('Please select an option and try again')
