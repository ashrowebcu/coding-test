
def checkFizz(number):
    if number % 3 == 0:
        return 'Fizz'
    return ''

def checkBuzz(number):
    if number % 5 == 0:
        return 'Buzz'
    return ''


if __name__ == "__main__":
    for x in range(500):
        s = checkFizz(x) + checkBuzz(x)
        if not s:
            print(x)
        else:
            print(s)