import requests, sys, csv, json

def getRequest(key, query):
    return json.loads(requests.get('http://www.omdbapi.com/?apikey=' + key + '&s=' + query).content)

if __name__ == "__main__":
    print('OMDb API search bot')
    print('Enter a search term:')
    input1 = input('> ')
    if input is None:
        print('Please enter a search term!')
        sys.exit()
    print('Enter your api key:')
    input2 = input('> ')
    if input2 is None:
        print('Please enter the API key!')
        sys.exit()
    results = getRequest(input2, input1)
    with open('out.csv', 'w', newline='') as output:
        writer = csv.writer(output, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Title', 'Year', 'imdbID', 'Type', 'Poster'])
        for result in results["Search"]:
            writer.writerow([result["Title"],result["Year"],result["imdbID"],result["Type"],result["Poster"]])
    print('done')